/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jta.manager.itest.propertiesbased.impl;

import org.amdatu.jta.ManagedTransactional;
import org.amdatu.jta.Transactional;
import org.amdatu.jta.manager.itest.testbundle.InterfaceBasedTestService;

@Transactional
public class InterfaceBasedTestServiceImpl extends BaseTestServiceImpl implements ManagedTransactional, InterfaceBasedTestService{

	@Override
	public Class<?>[] getManagedInterfaces() {
		return new Class[]{InterfaceBasedTestService.class};
	}

}
