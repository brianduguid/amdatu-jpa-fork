/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jta.manager.itest;

import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.amdatu.jta.manager.itest.testbundle.InterfaceBasedTestService;
import org.amdatu.jta.manager.itest.testbundle.TestEntity;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InterfaceBasedManagedTransactionsTest {

    private volatile InterfaceBasedTestService instance;

    @Before
    public void setUp() throws Exception {
        configure(this).add(createFactoryConfiguration("org.amdatu.jpa.datasourcefactory").set("userName", "sa").set("password", "sa")
            .set("driverClassName", "org.h2.Driver").set("jdbcUrl", "jdbc:h2:mem:test")
            .set("name", "ManagedTestDs").set("managed", "true").setSynchronousDelivery(true))
        .add(createFactoryConfiguration("org.amdatu.jpa.datasourcefactory").set("userName", "sa").set("password", "sa")
            .set("driverClassName", "org.h2.Driver").set("jdbcUrl", "jdbc:h2:mem:test")
            .set("name", "TestDs").setSynchronousDelivery(true))
        .add(createServiceDependency().setService(InterfaceBasedTestService.class).setRequired(true))
            .setTimeout(10, TimeUnit.SECONDS).apply();
    }
    
    @After
    public void tearDown(){
        cleanUp(this);
    }

    // We only have to test if a persisted entity can be found, if so, a transaction must have been active.
    @Test
    public void testPersist() {
        instance.save(new TestEntity("test"));

        List<TestEntity> list = instance.list();
        assertEquals(1, list.size());
    }

}
