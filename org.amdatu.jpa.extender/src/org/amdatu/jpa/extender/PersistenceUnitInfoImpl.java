/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.jpa.extender;

import static org.amdatu.jpa.extender.DelegatingEntityManager.NON_TRANSACTIONAL_ENTITY_MANAGER_ACCESS;
import static org.osgi.service.jpa.EntityManagerFactoryBuilder.JPA_UNIT_NAME;
import static org.osgi.service.jpa.EntityManagerFactoryBuilder.JPA_UNIT_PROVIDER;
import static org.osgi.service.jpa.EntityManagerFactoryBuilder.JPA_UNIT_VERSION;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceProvider;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;
import javax.transaction.TransactionManager;
import javax.transaction.TransactionSynchronizationRegistry;

import org.amdatu.database.schemamigration.SchemaMigrationException;
import org.amdatu.database.schemamigration.SchemaMigrationService;
import org.apache.felix.dm.Component;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.Bundle;
import org.osgi.framework.ServiceReference;
import org.osgi.framework.hooks.weaving.WeavingHook;
import org.osgi.framework.wiring.BundleWiring;
import org.osgi.service.log.LogService;

public class PersistenceUnitInfoImpl implements PersistenceUnitInfo {

    private static final String MANAGED_SCHEMA_PROPERTIES_NAMESPACE = "org.amdatu.database.schemamigration";

    private static final String MANAGED_SCHEMA_ENABLED_PROPERTY = MANAGED_SCHEMA_PROPERTIES_NAMESPACE + ".enabled";

    private static final String MANAGED_SCHEMA_LOCATIONS = MANAGED_SCHEMA_PROPERTIES_NAMESPACE + ".locations";

    private static final String MIGRATION_SERVICE_FILTER_PROPERTY = MANAGED_SCHEMA_PROPERTIES_NAMESPACE + ".migrationServiceFilter";
    
    private volatile List<ClassTransformer> classTransformers = new ArrayList<>();
    
    private final Bundle m_bundle;
    private final PersistenceDescriptor m_persistenceDescriptor;
    private final List<Component> m_services = new ArrayList<>();

    private volatile DataSource m_nonJtaDataSource;
    private volatile DataSource m_jtaDataSource;
    private volatile PersistenceProvider m_persistenceProvider;
    private volatile ServiceReference<PersistenceProvider> m_persistenceProviderRef;
    private volatile SchemaMigrationService m_schemaMigrationService;

    private String m_schemaVersion;

    public PersistenceUnitInfoImpl(Bundle bundle, PersistenceDescriptor persistenceDescriptor) {
        this.m_bundle = bundle;
        this.m_persistenceDescriptor = persistenceDescriptor;
    }

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void init(Component component){
        String migrationServiceFilter = (String) getProperties().get(MIGRATION_SERVICE_FILTER_PROPERTY);
        
        if (managedSchemaEnabled()) {
            component.add(component.getDependencyManager().createServiceDependency()
                .setService(SchemaMigrationService.class, migrationServiceFilter).setRequired(true));
        }
    }

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void start(Component component) {
        if (managedSchemaEnabled()) {
            migrateSchema();
        }

        DependencyManager dependencyManager = new DependencyManager(m_bundle.getBundleContext());
        Properties properties = new Properties();
        properties.put("pb", m_bundle.getSymbolicName());
        properties.put("pu", getPersistenceUnitName());
        String weavingHookFilter = String.format("(&(pb=%s)(pu=%s))", m_bundle.getSymbolicName(), getPersistenceUnitName());

        Component weavingHookComponent = dependencyManager
            .createComponent()
            .setInterface(WeavingHook.class.getName(), properties)
            .setImplementation(new EnhanceWeavingHook(this))
            .add(dependencyManager.createServiceDependency().setService(LogService.class)
                .setRequired(false));


        Properties props = new Properties();

        props.put(JPA_UNIT_NAME, getPersistenceUnitName());
        props.put(JPA_UNIT_PROVIDER, getPersistenceProvider().getClass().getName());
        props.put(JPA_UNIT_VERSION, m_bundle.getVersion());

        EntityManagerFactory entityManagerFactory = m_persistenceProvider.createContainerEntityManagerFactory(this, new HashMap<>());
        
        
        Component entityManagerFactoryComponent = dependencyManager
            .createComponent()
            .setInterface(EntityManagerFactory.class.getName(), props)
            .setImplementation(new NonClosingEntityManagerFactory(entityManagerFactory))
            .add(dependencyManager.createServiceDependency().setService(TransactionManager.class)
                .setRequired(true))
            .add(dependencyManager.createServiceDependency().setService(WeavingHook.class, weavingHookFilter).setRequired(true));;
        

        String filter = String.format("(%s=%s)", JPA_UNIT_NAME, getPersistenceUnitName());
        Component entityManagerComponent = dependencyManager
            .createComponent()
            .setInterface(EntityManager.class.getName(), props)
            .setImplementation(
                new DelegatingEntityManager(entityManagerFactory, allowNonTransactionalEntityManagerAccess()))
            .add(dependencyManager.createServiceDependency().setService(TransactionManager.class).setRequired(true))
            .add(
                dependencyManager.createServiceDependency().setService(TransactionSynchronizationRegistry.class)
                    .setRequired(true))
            .add(dependencyManager.createServiceDependency().setService(WeavingHook.class, weavingHookFilter).setRequired(true));

        synchronized (m_services) {
            m_services.add(weavingHookComponent);
            m_services.add(entityManagerFactoryComponent);
            m_services.add(entityManagerComponent);
            
            for (Component c: m_services) {
                dependencyManager.add(c);
            }
        }
    }

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void stop() {
        synchronized (m_services) {
            for (Iterator<Component> iterator = m_services.iterator(); iterator.hasNext();) {
                Component component = iterator.next();
                component.getDependencyManager().remove(component);
                iterator.remove();
            }
        }
    }

    private boolean managedSchemaEnabled() {
        String isManagedSchema = (String) getProperties().get(MANAGED_SCHEMA_ENABLED_PROPERTY);
        if (isManagedSchema != null && Boolean.valueOf(isManagedSchema)) {
            return true;
        }
        return false;
    }

    private void migrateSchema() {
        String migrationScriptDir = (String) getProperties().get(MANAGED_SCHEMA_LOCATIONS);
        if (migrationScriptDir == null) {
            throw new IllegalStateException("Migration script dir should not be null");
        }

        DataSource dataSource = getNonJtaDataSource();
        if (dataSource == null) {
            dataSource = getJtaDataSource();
        }

        if (dataSource == null) {
            throw new IllegalStateException("No datasource available for schema migration");
        }

        Properties properties = new Properties();
        Enumeration<?> propertyNames = getProperties().propertyNames();
        while (propertyNames.hasMoreElements()) {
            String name = (String) propertyNames.nextElement();

            if (name.startsWith(MANAGED_SCHEMA_PROPERTIES_NAMESPACE)) {
                properties.put(name.substring(MANAGED_SCHEMA_PROPERTIES_NAMESPACE.length() + 1),
                    getProperties().get(name));
            }
        }

        try {
            m_schemaVersion = m_schemaMigrationService.migrate(dataSource, m_bundle, migrationScriptDir, properties);
        }
        catch (SchemaMigrationException e) {
            throw new RuntimeException("Schema migration failed", e);
        }
    }

    @Override
    public void addTransformer(ClassTransformer classTransformer) {
        classTransformers.add(classTransformer);
    }

    @Override
    public boolean excludeUnlistedClasses() {
        return true;
    }

    @Override
    public ClassLoader getClassLoader() {
        return m_bundle.adapt(BundleWiring.class).getClassLoader();
    }

    @Override
    public List<URL> getJarFileUrls() {
        return Collections.emptyList();
    }

    @Override
    public DataSource getJtaDataSource() {
        return m_jtaDataSource;
    }

    @Override
    public List<String> getManagedClassNames() {
        return m_persistenceDescriptor.getClasses();
    }

    @Override
    public List<String> getMappingFileNames() {
        return Collections.emptyList();
    }

    @Override
    public ClassLoader getNewTempClassLoader() {

        return new ClassLoader() {

            @SuppressWarnings("rawtypes")
            private Map<String, Class> loaded = new HashMap<>();

            @Override
            public Class<?> loadClass(String name) throws ClassNotFoundException {
                if (loaded.containsKey(name)) {
                    return loaded.get(name);
                }

                String clazzName = name;
                if (name.contains("$")) {
                    clazzName = name.split("\\$")[0];
                }

                if (!!!m_persistenceDescriptor.getClasses().contains(clazzName)) {
                    /*
                     * Delegate class loading to the bundle ClassLoader in case
                     * a the class that is loaded is not an Entity class
                     */
                    return getClassLoader().loadClass(name);
                }

                Class<?> clazz = findClass(name);
                loaded.put(name, clazz);
                return clazz;
            }

            @Override
            protected Class<?> findClass(String className) throws ClassNotFoundException {
                String classResName = className.replace('.', '/').concat(".class");

                InputStream is = null;
                URL url = m_bundle.getResource(classResName);
                try {
                    is = (url == null) ? null : url.openStream();
                }
                catch (IOException e) {

                }

                if (is == null) {
                    throw new ClassNotFoundException(className);
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                byte[] buff = new byte[4096];
                try {
                    try {
                        int read = is.read(buff);
                        while (read > 0) {
                            baos.write(buff, 0, read);
                            read = is.read(buff);
                        }
                    }
                    finally {
                        is.close();
                    }
                }
                catch (IOException ioe) {
                    throw new ClassNotFoundException(className, ioe);
                }

                buff = baos.toByteArray();
                Class<?> defineClass = defineClass(className, buff, 0, buff.length);
                return defineClass;
            }
        };
    }

    @Override
    public DataSource getNonJtaDataSource() {
        return m_nonJtaDataSource;
    }

    @Override
    public String getPersistenceProviderClassName() {
        return m_persistenceDescriptor.getProvider();
    }

    @Override
    public String getPersistenceUnitName() {
        return m_persistenceDescriptor.getUnitName();
    }

    @Override
    public URL getPersistenceUnitRootUrl() {
        URL resource = m_bundle.getResource("/");
        return resource;
    }

    @Override
    public String getPersistenceXMLSchemaVersion() {
        return "2.0";
    }

    @Override
    public Properties getProperties() {
        return m_persistenceDescriptor.getProperties();
    }

    @Override
    public SharedCacheMode getSharedCacheMode() {
        // TODO
        return SharedCacheMode.UNSPECIFIED;
    }

    @Override
    public PersistenceUnitTransactionType getTransactionType() {
        return PersistenceUnitTransactionType.JTA;
    }

    @Override
    public ValidationMode getValidationMode() {
        // TODO
        return ValidationMode.NONE;
    }

    public List<ClassTransformer> getClassTransformers() {
        return classTransformers;
    }

    public PersistenceDescriptor getPersistenceDescriptor() {
        return m_persistenceDescriptor;
    }

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void setNonJtaDataSource(DataSource dataSource) {
        this.m_nonJtaDataSource = dataSource;
    }

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void removeNonJtaDataSource(DataSource dataSource) {
        this.m_nonJtaDataSource = null;
    }

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void setJtaDataSource(DataSource dataSource) {
        this.m_jtaDataSource = dataSource;
    }

    @SuppressWarnings("unused" /* dependency manager callback */)
    private void removeJtaDataSource(DataSource dataSource) {
        this.m_jtaDataSource = null;
    }

    public void addPersistenceProvider(ServiceReference<PersistenceProvider> ref,
        PersistenceProvider persistenceProvider) {
        this.m_persistenceProvider = persistenceProvider;
        this.m_persistenceProviderRef = ref;
    }

    public void removePersistenceProvider(ServiceReference<PersistenceProvider> ref) {
        this.m_persistenceProvider = null;
        this.m_persistenceProviderRef = null;
    }

    public Bundle getPersistenceProviderBundle() {
        return m_persistenceProviderRef.getBundle();
    }

    public PersistenceProvider getPersistenceProvider() {
        return m_persistenceProvider;
    }

    private boolean allowNonTransactionalEntityManagerAccess() {
        String amdatuJpaProperty = (String) getProperties().get(NON_TRANSACTIONAL_ENTITY_MANAGER_ACCESS);
        if (amdatuJpaProperty != null) {
            return Boolean.valueOf(amdatuJpaProperty);
        }

        String openJpaProperty = (String) getProperties().get("openjpa.Multithreaded");
        if (openJpaProperty != null) {
            return Boolean.valueOf(openJpaProperty);
        }

        return false;
    }

    
    @Override
    public String toString() {
        return "PersistenceUnitInfoImpl [PersistenceUnit:"
            + getPersistenceUnitName() + 
            (managedSchemaEnabled()? "Schema version: " + m_schemaVersion:"")
            + "]";
    }
}